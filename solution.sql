II. Given the following data, provide the answer to the following:

A.List the books Authored by Marjorie Green.
	> The Busy Executives Database Guide
	> You Cam Combat Computer Stress!

B.List the books Authored by Michael OLeary.
	> Cooking with Computers

C.Write the author/s of "The Busy Executives Database Guide".
	> Green Marjorie
	> Bennet Abraham

D.Identify the publisher of "But Is It User Friendly?".
	> Algodata Infosystems

E.List the books published by Algodata Infosystems.
	> The Busy Executives Database Guide
	> Cooking with Computers
	> Straight Talk About Computers
	> But Is It User Friendly?
	> Secrets of Silicon Valley
	> Net Etiquette


III. Create SQL Syntax and Queries to create a database based on the ERD:
	 Database Name: blog_db
	 
	//Creating database blog_db
	MariaDB [(none)]> create database blog_db;
	Query OK, 1 row affected (0.002 sec)

	MariaDB [(none)]> show databases;
	+--------------------+
	| Database           |
	+--------------------+
	| blog_db            |
	| information_schema |
	| music_db           |
	| mysql              |
	| performance_schema |
	| phpmyadmin         |
	| test               |
	+--------------------+
	MariaDB [(none)]> use blog_db;
	Database changed

	//Creating table USERS
	MariaDB [blog_db]> Create table users (id int not null auto_increment, email varchar(100), password varchar(300), datetime_created datetime, PRIMARY KEY(id));
	Query OK, 0 rows affected (0.011 sec)

	MariaDB [blog_db]> describe users;
	+------------------+--------------+------+-----+---------+----------------+
	| Field            | Type         | Null | Key | Default | Extra          |
	+------------------+--------------+------+-----+---------+----------------+
	| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
	| email            | varchar(100) | YES  |     | NULL    |                |
	| password         | varchar(300) | YES  |     | NULL    |                |
	| datetime_created | datetime     | YES  |     | NULL    |                |
	+------------------+--------------+------+-----+---------+----------------+
	4 rows in set (0.013 sec)

	//Creating table POSTS
	MariaDB [blog_db]> Create table posts (id int not null auto_increment,title varchar(500), content varchar(5000), datetime_posted datetime, author_id int not null, PRIMARY KEY(id), CONSTRAINT fk_posts_user_id FOREIGN KEY (author_id) REFERENC
	ES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
	Query OK, 0 rows affected (0.021 sec)
	MariaDB [blog_db]> describe posts;
	+-----------------+---------------+------+-----+---------+----------------+
	| Field           | Type          | Null | Key | Default | Extra          |
	+-----------------+---------------+------+-----+---------+----------------+
	| id              | int(11)       | NO   | PRI | NULL    | auto_increment |
	| title           | varchar(500)  | YES  |     | NULL    |                |
	| content         | varchar(5000) | YES  |     | NULL    |                |
	| datetime_posted | datetime      | YES  |     | NULL    |                |
	| author_id       | int(11)       | NO   | MUL | NULL    |                |
	+-----------------+---------------+------+-----+---------+----------------+
	5 rows in set (0.013 sec)

	//Creating table post_comments
	MariaDB [blog_db]> create table post_comments (id int not null auto_increment, content varchar(5000), datetime_commented datetime, post_id int not null, user_id int not null, PRIMARY KEY (id), CONSTRAINT fk_post_comments_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT, CO
	NSTRAINT fk_post_comments_post_id FOREIGN KEY (post_id) REFERENCES posts (id) ON UPDATE CASCADE ON DELETE RESTRICT);
	Query OK, 0 rows affected (0.015 sec)

	MariaDB [blog_db]> describe post_comments;
	+--------------------+---------------+------+-----+---------+----------------+
	| Field              | Type          | Null | Key | Default | Extra          |
	+--------------------+---------------+------+-----+---------+----------------+
	| id                 | int(11)       | NO   | PRI | NULL    | auto_increment |
	| content            | varchar(5000) | YES  |     | NULL    |                |
	| datetime_commented | datetime      | YES  |     | NULL    |                |
	| post_id            | int(11)       | NO   | MUL | NULL    |                |
	| user_id            | int(11)       | NO   | MUL | NULL    |                |
	+--------------------+---------------+------+-----+---------+----------------+
	5 rows in set (0.013 sec)

	//Creating table post_likes
	MariaDB [blog_db]> create table post_likes (id int not null auto_increment, datetime_liked datetime, post_id int not null, user_id int not null, PRIMARY KEY (id), CONSTRAINT fk_post_likes_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_likes_post_id FO
	REIGN KEY (post_id) REFERENCES posts (id) ON UPDATE CASCADE ON DELETE RESTRICT);
	Query OK, 0 rows affected (0.019 sec)

	MariaDB [blog_db]> describe post_likes;
	+----------------+----------+------+-----+---------+----------------+
	| Field          | Type     | Null | Key | Default | Extra          |
	+----------------+----------+------+-----+---------+----------------+
	| id             | int(11)  | NO   | PRI | NULL    | auto_increment |
	| datetime_liked | datetime | YES  |     | NULL    |                |
	| post_id        | int(11)  | NO   | MUL | NULL    |                |
	| user_id        | int(11)  | NO   | MUL | NULL    |                |
	+----------------+----------+------+-----+---------+----------------+
	4 rows in set (0.012 sec)
